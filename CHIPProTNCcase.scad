printable=0;
show_tnc=0;
show_bottom=0;
show_top=0;
show_stand=1;
side_cut=1;

screw_r=1;
screw_d=screw_r*2;
screw_l=20;
screw_wall=0.75;
board_l=79;
board_w=48.5;
board_hw=board_w/2;
board_top=5;
board_bot=16;
board_thickness=2;
tab_w=2;
wall_w=1;

if( show_tnc == 1 )
    translate([0,42+1.5*wall_w,0]) 
        import( "CHIPProTNC.stl" );

module face( height ) {
    difference() {
    hull() {
    $fn=60;
    translate([board_hw+screw_r,height,board_top+screw_r])
        rotate([90,0,0])
            cylinder(h=height,r=screw_r+screw_wall );
    translate([-(board_hw+screw_r),height,board_top+screw_r])
        rotate([90,0,0])
            cylinder(h=height,r=screw_r+screw_wall );
    translate([board_hw+screw_r,height,-(board_bot+screw_r)])
        rotate([90,0,0])
            cylinder(h=height,r=screw_r+screw_wall );
    translate([-(board_hw+screw_r),height,-(board_bot+screw_r)])
        rotate([90,0,0])
            cylinder(h=height,r=screw_r+screw_wall );
    translate([0,height,0])
        rotate([90,0,0])
    linear_extrude( height=height )
            polygon( [[ board_hw+screw_d, -(board_bot+screw_d) ], [ board_hw+screw_d,board_top+screw_d ], [-(board_hw+screw_d),board_top+screw_d], [-(board_hw+screw_d), -(board_bot+screw_d) ], [ board_hw+screw_d,-(board_bot+screw_d)]] );
    }
    translate([-17,wall_w/2,-16])
        rotate([90,0,0]) 
            linear_extrude( height=4 )
                scale( [ 1.5,1.5, 1 ] )
                    import( "Akkea-Logo.dxf" );
    }
}

module shell() {
    difference() {
    face( board_l+wall_w );

    union() {
    if( side_cut ==0 )
    {    
    $fn=60;
    translate([board_hw+screw_r,board_l+1,board_top+screw_r])
        rotate([90,0,0])
            cylinder(h=screw_l,r=screw_r );
    translate([-(board_hw+screw_r),board_l+1,board_top+screw_r])
        rotate([90,0,0])
            cylinder(h=screw_l,r=screw_r );
    translate([board_hw+screw_r,board_l+1,-(board_bot+screw_r)])
        rotate([90,0,0])
            cylinder(h=screw_l,r=screw_r );
    translate([-(board_hw+screw_r),board_l+1,-(board_bot+screw_r)])
        rotate([90,0,0])
            cylinder(h=screw_l,r=screw_r );
    }
    translate([0,board_l+wall_w+1,0])
        rotate([90,0,0])
    linear_extrude( height=board_l+wall_w )
            polygon( [[ board_hw, -board_bot ], 
                [board_hw,-(board_thickness/2+ tab_w)], 
                [board_hw-tab_w, -(board_thickness/2)], 
                [board_hw,-(board_thickness/2)], 
                [board_hw,board_thickness/2], 
                [board_hw-tab_w, board_thickness/2], 
                [board_hw,board_thickness/2+tab_w],  
                [ board_hw,board_top ], 
                [-board_hw, board_top], 
                [-board_hw,-(board_thickness/2+tab_w)], 
                [-(board_hw-tab_w), -(board_thickness/2)], 
                [-board_hw,-(board_thickness/2)], 
                [-board_hw,board_thickness/2], 
                [-(board_hw-tab_w), board_thickness/2], 
                [-board_hw,board_thickness/2+tab_w] , 
                [-board_hw, -board_bot ], 
                [ board_hw,-board_bot]] );
      
    }
    }
}

module button() {
    hull() {
        $fn=60;
        translate([ 2, 0, wall_w*1.5/2 ])
            cube([6,6,wall_w*1.5], center=true);
        cylinder(r=3, h=wall_w*1.5 );
    }
}

module bottom() {
    difference() {
        shell();
        union() {
        // micro usb connector
        translate([0.75,0,3])
            cube([ 12, 5, 7 ], center=true );
        translate([ 19, 7, board_top-0.1])
            scale([1.3, 1.3, 2])
                button();
        // LED hole
        $fn=60;
        translate([ 11, 32.5+wall_w, board_top-0.1])
            scale([1.1, 1.1, 2])
                cylinder(r=2, h=wall_w*2 );
        }
    }
}

if( show_top ) {
    // power button
    translate([ 19, 7, board_top+wall_w*0.5+screw_wall ])
    difference() {
    union() {
        button();
        
        //hinge
        translate([ 4, 0, wall_w/4 ])
            cube([6,6,wall_w/2], center=true);
        //drop bar
        hull() {
        //translate([ 1.25, -2.9, -board_top+2.4])
            //cube([2,6,board_top-3], center=true);
        translate([ 1.25, 0, -1.25])
            cube([2,3,board_top-2], center=true);
        }
    }

    r=screw_r*2;
    $fn=60;
    translate([6,-4,-1])
        cylinder(h=4,r=r );
    }
}

module top() {
    clearance=0.5;
    
    difference() {
    // outside
    union() {
    face( 2*wall_w );
    if( side_cut == 0 )
    {
    translate([0,wall_w*3,0])
        rotate([90,0,0])
    linear_extrude( height=wall_w )
            polygon( [[ board_hw-0.5, -(board_bot-0.5) ], [ board_hw-0.5,board_top-0.5 ], [-(board_hw-0.5),board_top-0.5], [-(board_hw-0.5), -(board_bot-0.5) ], [ board_hw-0.5,-(board_bot-0.5)]] );
    }
    }
    union() {
    //antenna hole
    translate([18.75,wall_w*4,-8.5])
        rotate([90,0,0] )
            cylinder( r=6, h=wall_w*2 );
    translate([18.75,wall_w*3,-8.5])
        rotate([90,0,0] )
            cylinder( r=3.8, h=wall_w*4 );
    if( side_cut == 0 ) {
    // Alignment tabs        
    translate([0,wall_w*4,0])
        rotate([90,0,0])
    linear_extrude( height=wall_w*2 )
            polygon( [[ board_hw, -(board_bot-clearance*3) ], 
                [ board_hw,board_top-clearance*3 ], 
                [-(board_hw),board_top-clearance*3], 
                [-(board_hw), -(board_bot-clearance*3) ], 
                [board_hw ,-(board_bot-clearance*3)]] );
    
    //screw holes
    $fn=60;
    translate([board_hw+screw_r,wall_w*3,board_top+screw_r])
        rotate([90,0,0])
            cylinder(h=wall_w*4,r=screw_r+0.2 );
    translate([-(board_hw+screw_r),wall_w*3,board_top+screw_r])
        rotate([90,0,0])
            cylinder(h=wall_w*4,r=screw_r+0.2 );
    translate([board_hw+screw_r,wall_w*3,-(board_bot+screw_r)])
        rotate([90,0,0])
            cylinder(h=wall_w*4,r=screw_r+0.2 );
    translate([-(board_hw+screw_r),wall_w*3,-(board_bot+screw_r)])
        rotate([90,0,0])
            cylinder(h=wall_w*4,r=screw_r+0.2 );
    }
    }
    }
}

module full_shell() {
    bottom();

    translate([0,board_l+wall_w*3,0])
                rotate([180,180,0])
                    top();
}

module screw_holes() {
    $fn=60;
    l = board_top+board_bot;
    translate([board_hw+screw_r,wall_w*3,-(board_bot-wall_w*3)])
        cylinder(h=l,r=screw_r );
    translate([-(board_hw+screw_r),wall_w*3,-(board_bot-wall_w*3)])
        cylinder(h=l,r=screw_r );
    translate([board_hw+screw_r,(board_l+wall_w)/2,-(board_bot-wall_w*3)])
        cylinder(h=l,r=screw_r );
    translate([-(board_hw+screw_r),(board_l+wall_w)/2,-(board_bot-wall_w*3)])
        cylinder(h=l,r=screw_r );
    translate([board_hw+screw_r,board_l-wall_w,-(board_bot-wall_w*3)])
        cylinder(h=l,r=screw_r );
    translate([-(board_hw+screw_r),board_l-wall_w,-(board_bot-wall_w*3)])
        cylinder(h=l,r=screw_r );
    // countersink
    depth=board_top+wall_w;
    r=screw_r*2;
    translate([board_hw+screw_r,wall_w*3,depth])
        cylinder(h=l,r=r );
    translate([-(board_hw+screw_r),wall_w*3,depth])
        cylinder(h=l,r=r );
    translate([board_hw+screw_r,(board_l+wall_w)/2,depth])
        cylinder(h=l,r=r );
    translate([-(board_hw+screw_r),(board_l+wall_w)/2,depth])
        cylinder(h=l,r=r );
    translate([board_hw+screw_r,board_l-wall_w,depth])
        cylinder(h=l,r=r );
    translate([-(board_hw+screw_r),board_l-wall_w,depth])
        cylinder(h=l,r=r );
} 
 
module top_shell() {
    difference() {
        full_shell();
        union() {
        translate([ 0, board_l/2, -((board_top+board_bot-screw_wall)/2)])
            cube([board_w+wall_w*6,board_l+wall_w*8,board_bot+wall_w*4], center=true);
        screw_holes();    
        }
    }
}

module bottom_shell() {
    difference() {
        full_shell();
        union() {
            top_shell();
            screw_holes();
        }
    }
}

if( side_cut == 0 )
{
    if( show_bottom ==  1 )
        bottom();

    if( show_top == 1 )
    {
        if( printable == 1 ) 
            translate([0,0,board_top+board_bot+10]) top();
        else
            translate([0,board_l+wall_w*3,0])
                rotate([180,180,0])
                    top();
    }
}
else
{
    if( show_top )
        top_shell();
    
    if( show_bottom )
    {
        if( printable )
            translate([ board_w+wall_w *10,0, -(board_top+wall_w+screw_wall)] )
                rotate([0,180,0])
                    bottom_shell();
        else
            bottom_shell();
    }
}

module stand() {
    difference() {
        union() {
        translate([-(board_hw+wall_w*4),-3,-(board_bot+wall_w*4)])
        cube([board_w+wall_w*8, 20, board_top+board_bot+wall_w*8]);
        translate([0,7,-27])    
        rotate([45,0,0])
        union() {
            translate([0,-10,0])
            cube([board_w+wall_w*8, 2, 2*(board_top+board_bot)], center=true );
            translate([0,-4,12])
            cube([board_w+wall_w*8, 10, 6], center=true);
        }
        }
        union() {
        translate([-(board_hw+wall_w*3),-2,-(board_bot+wall_w*3)])
        cube([board_w+wall_w*6, 20, board_top+board_bot+wall_w*6]);
        translate([-(board_hw/2),-4,-(board_bot)])
        cube([board_hw, 25, board_top+board_bot+wall_w*6]);
        }
    }
}

if( show_stand )
    stand();